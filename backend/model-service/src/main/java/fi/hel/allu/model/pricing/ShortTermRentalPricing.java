package fi.hel.allu.model.pricing;

import fi.hel.allu.common.util.CalendarUtil;
import fi.hel.allu.common.domain.types.ApplicationKind;
import fi.hel.allu.common.domain.types.ApplicationType;
import fi.hel.allu.common.domain.types.ChargeBasisUnit;
import fi.hel.allu.model.dao.PricingDao;
import fi.hel.allu.model.domain.Application;
import fi.hel.allu.model.domain.PricingKey;
import fi.hel.allu.model.domain.ShortTermRental;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class ShortTermRentalPricing extends Pricing {
  private final Application application;
  private final PricingExplanator explanationService;
  private final PricingDao pricingDao;
  private final double applicationArea;
  private final boolean customerIsCompany;
  private final DecimalFormat decimalFormat;

  // Various price constants for short term rental
  private static final int LONG_TERM_DISCOUNT_LIMIT = 14; // how many days
                                                          // before discount?

  /*
   * Invoice line string constants
   */
  static class InvoiceLines {
    static final String ART = "Taideteos";
    static final String BANDEROL_NONCOMMERCIAL = "Banderollit silloissa, ei-kaupallinen";
    static final String BANDEROL_COMMERCIAL = "Banderollit silloissa, kaupallinen";
    static final String BENJI = "Benji-hyppylaite";
    static final String OTHER_SHORT_TERM_RENTAL = "Muu lyhytaikainen maanvuokraus";
    static final String PROMOTION_OR_SALES_SMALL = "Korkeintaan 0,8 m seinästä";
    static final String PROMOTION_OR_SALES_LARGE = "%s €/m²/kk + alv, yli 0,8 m seinästä";
    static final String STORAGE_AREA = "Varastoalue";
    static final String URBAN_FARMING = "Kaupunkiviljelypaikka yhdistyksille ja yhteisöille";
    static final String KESKUSKATU_SALES = "%s €/päivä/alkava 10 m² + alv";
    static final String SUMMER_THEATER = "%s €/toimintakuukausi";
    static final String DOG_TRAINING_FIELD_ORG = "Vuosivuokra yhdistyksille %s €/vuosi (2h/vk)";
    static final String DOG_TRAINING_FIELD_COM = "Vuosivuokra yrityksille %s €/vuosi (2h/vk)";
    static final String DOG_TRAINING_EVENT_ORG = "Koirankoulutustapahtuma, järjestäjänä yhdistys";
    static final String DOG_TRAINING_EVENT_COM = "Koirankoulutustapahtuma, järjestäjänä yritys";
    static final String SMALL_ART_AND_CULTURE = "Pienimuotoinen kaupallinen taide- ja kulttuuritoiminta";
    static final String SEASON_SALES = "%s €/päivä/alkava 10 m² + alv";
    static final String CIRCUS = "%s €/päivä + alv";
  }

  public ShortTermRentalPricing(Application application, PricingExplanator explanationService, PricingDao pricingDao, double applicationArea, boolean customerIsCompany) {
    super();
    this.application = application;
    this.applicationArea = applicationArea;
    this.customerIsCompany = customerIsCompany;
    this.explanationService = explanationService;
    this.pricingDao = pricingDao;
    this.decimalFormat = new DecimalFormat("#.00");
    final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
    symbols.setDecimalSeparator(',');
    decimalFormat.setDecimalFormatSymbols(symbols);
  }


  public void calculatePrice() {
    if (application.getKindsWithSpecifiers() == null) {
      return;
    }
    application.getKindsWithSpecifiers().keySet().forEach(kind -> calculatePrice(kind));
  }

  private void calculatePrice(ApplicationKind kind) {
    switch (kind) {
    case ART:
      // Free event
      setPriceInCents(0);
      addChargeBasisEntry(ChargeBasisTag.ShortTermRentalArt(), ChargeBasisUnit.PIECE, 1.0, 0,
          InvoiceLines.ART, 0, explanationService.getExplanation(application));
      break;
    case SMALL_ART_AND_CULTURE:
      // Free event
      setPriceInCents(0);
      addChargeBasisEntry(ChargeBasisTag.ShortTermRentalSmallArtAndCulture(), ChargeBasisUnit.PIECE, 1.0, 0,
          InvoiceLines.SMALL_ART_AND_CULTURE, 0, explanationService.getExplanation(application));
      break;
    case BENJI:
      // 320 EUR/day
      updatePricePerUnit(ChargeBasisTag.ShortTermRentalBenji(), ChronoUnit.DAYS,
          getPrice(PricingKey.BENJI_DAILY_PRICE), InvoiceLines.BENJI);
      break;
    case BRIDGE_BANNER:
      // Non-commercial organizer: 150 EUR/week
      // Commercial organizer: 750 EUR/week
      if (isCommercial()) {
        updatePricePerUnit(ChargeBasisTag.ShortTermRentalBridgeBanner(), ChronoUnit.WEEKS,
            getPrice(PricingKey.BRIDGE_BANNER_WEEKLY_PRICE_COMMERCIAL), InvoiceLines.BANDEROL_COMMERCIAL);
      } else {
        updatePricePerUnit(ChargeBasisTag.ShortTermRentalBridgeBanner(), ChronoUnit.WEEKS,
            getPrice(PricingKey.BRIDGE_BANNER_WEEKLY_PRICE_NONCOMMERCIAL),
            InvoiceLines.BANDEROL_NONCOMMERCIAL);
      }
      break;
    case CIRCUS: {
      final int price = getPrice(PricingKey.CIRCUS_DAILY_PRICE);
      updatePricePerUnit(ChargeBasisTag.ShortTermRentalCircus(), ChronoUnit.DAYS,
          price, priceText(price, InvoiceLines.CIRCUS));
      break;
    }
    case DOG_TRAINING_EVENT:
      // Associations: 50 EUR/event
      // Companies: 100 EUR/event
      if (customerIsCompany) {
        final int price = getPrice(PricingKey.DOG_TRAINING_EVENT_COMPANY_PRICE);
        setPriceInCents(price);
        addChargeBasisEntry(ChargeBasisTag.ShortTermRentalDogTrainingEvent(), ChargeBasisUnit.PIECE, 1,
            price, InvoiceLines.DOG_TRAINING_EVENT_COM, price, explanationService.getExplanation(application));
      } else {
        final int price = getPrice(PricingKey.DOG_TRAINING_EVENT_ASSOCIATION_PRICE);
        setPriceInCents(price);
        addChargeBasisEntry(ChargeBasisTag.ShortTermRentalDogTrainingEvent(), ChargeBasisUnit.PIECE, 1,
            price, InvoiceLines.DOG_TRAINING_EVENT_ORG, price, explanationService.getExplanation(application));
      }
      break;
    case DOG_TRAINING_FIELD:
      // Associations: 100 EUR/year
      // Companies: 300 EUR/year
      if (customerIsCompany) {
        final int price = getPrice(PricingKey.DOG_TRAINING_FIELD_YEARLY_COMPANY);
        updatePricePerUnit(ChargeBasisTag.ShortTermRentalDogTrainingField(), ChronoUnit.YEARS,
            price, priceText(price, InvoiceLines.DOG_TRAINING_FIELD_COM));
      } else {
        final int price = getPrice(PricingKey.DOG_TRAINING_FIELD_YEARLY_ASSOCIATION);
        updatePricePerUnit(ChargeBasisTag.ShortTermRentalDogTrainingField(), ChronoUnit.YEARS,
            price, priceText(price, InvoiceLines.DOG_TRAINING_FIELD_ORG));
      }
      break;
    case KESKUSKATU_SALES: {
      // 50 EUR/day/starting 10 sqm
      final int price = getPrice(PricingKey.KESKUSKATU_SALES_TEN_SQM_PRICE);
      updatePriceByTimeAndArea(price, ChronoUnit.DAYS, 10, false,
          priceText(price, InvoiceLines.KESKUSKATU_SALES), null, ChargeBasisTag.ShortTermRentalKeskuskatuSales(), null);
      break;
    }
    case OTHER:
      // Handler should set the price override
      break;
    case PROMOTION_OR_SALES:
      // at max 0.8 m from a wall: free of charge
      // over 0.8m from a wall: 2 EUR/sqm/kk
      ShortTermRental str = (ShortTermRental) application.getExtension();
      if (str != null && Optional.ofNullable(str.getBillableSalesArea()).orElse(false) == true) {
        final int price = getPrice(PricingKey.PROMOTION_OR_SALES_MONTHLY);
        updatePriceByTimeAndArea(price, ChronoUnit.MONTHS, 1, false,
            priceText(price, InvoiceLines.PROMOTION_OR_SALES_LARGE), null,
            ChargeBasisTag.ShortTermRentalPromotionOrSales(), null);
      } else {
        // free of charge
        addChargeBasisEntry(ChargeBasisTag.ShortTermRentalPromotionOrSales(), ChargeBasisUnit.PIECE, 1, 0,
            InvoiceLines.PROMOTION_OR_SALES_SMALL, 0, explanationService.getExplanation(application));
        setPriceInCents(0);
      }
      break;
    case SEASON_SALE: {
      // 50 EUR/day/starting 10 sqm
      final int price = getPrice(PricingKey.SEASON_SALE_TEN_SQM_PRICE);
      updatePriceByTimeAndArea(price, ChronoUnit.DAYS, 10, false, priceText(price, InvoiceLines.SEASON_SALES),
          null, ChargeBasisTag.ShortTermRentalSeasonSale(), null);
      break;
    }
    case STORAGE_AREA:
      // 0.50 EUR/sqm/month
      updatePriceByTimeAndArea(getPrice(PricingKey.STORAGE_AREA_MONTHLY_PRICE), ChronoUnit.MONTHS, 1, false,
          InvoiceLines.STORAGE_AREA, null, ChargeBasisTag.ShortTermRentalStorageArea(), null);
      break;
    case SUMMER_THEATER: {
      // 120 EUR/month
      final int price = getPrice(PricingKey.SUMMER_THEATER_YEARLY_PRICE);
      updatePricePerUnit(ChargeBasisTag.ShortTermRentalSummerTheater(), ChronoUnit.MONTHS,
          price, priceText(price, InvoiceLines.SUMMER_THEATER));
      break;
    }
    case URBAN_FARMING:
      updateUrbanFarmingPrice();
      break;
    default:
      break;
    }
  }

  /*
   * Calculate application's price using the application period. Price is
   * calculated as [centsPerUnit] * [starting units] and stored into
   * application.
   */
  private void updatePricePerUnit(ChargeBasisTag chargeBasisTag, ChronoUnit chronoUnit, int centsPerUnit,
      String chargeBasisText) {
    final int units = (int) CalendarUtil.startingUnitsBetween(application.getStartTime(), application.getEndTime(),
        chronoUnit);
    int priceInCents = centsPerUnit * units;
    addChargeBasisEntry(chargeBasisTag, toChargeBasisUnit(chronoUnit), units, centsPerUnit, chargeBasisText,
        priceInCents, explanationService.getExplanation(application));
    setPriceInCents(priceInCents);
  }

  private boolean isCommercial() {
    final ShortTermRental str = (ShortTermRental) application.getExtension();
    if (str != null && str.getCommercial() != null) {
      return str.getCommercial();
    }
    // if commercial-flag is not available, assume false
    return false;
  }

  /**
   * Update price based on application time and area
   *
   * @param priceInCents unit price in cents
   * @param pricePeriod billing time unit
   * @param priceArea billing area unit in square meters
   * @param longTermDiscount should long time discount be applied?
   * @param chargeBasisText explanation text for full-price charge basis entries
   * @param chargeBasisTextLongTerm explanation text for discounted charge basis
   *          entries
   * @param chargeBasisTag charge basis tag for full-price charge basis entries
   * @param chargeBasisTagLongTerm charge basis tag for discounted charge basis
   *          entries
   */
  private void updatePriceByTimeAndArea(int priceInCents,
      ChronoUnit pricePeriod, int priceArea, boolean longTermDiscount,
      String chargeBasisText, String chargeBasisTextLongTerm, ChargeBasisTag chargeBasisTag,
      ChargeBasisTag chargeBasisTagLongTerm) {
    final int numTimeUnits = (int) CalendarUtil.startingUnitsBetween(application.getStartTime(),
        application.getEndTime(), pricePeriod);

    final int numAreaUnits = (int) Math.ceil(applicationArea / priceArea);
    // How many time units are charged full price?
    int fullPriceUnits = longTermDiscount ? Math.min(numTimeUnits, LONG_TERM_DISCOUNT_LIMIT) : numTimeUnits;
    long price = numAreaUnits * fullPriceUnits * priceInCents;
    addChargeBasisEntry(chargeBasisTag, toChargeBasisUnit(pricePeriod), fullPriceUnits, numAreaUnits * priceInCents,
        chargeBasisText, (int) price,  explanationService.getExplanation(application));

    if (longTermDiscount == true && numTimeUnits > LONG_TERM_DISCOUNT_LIMIT) {
      // 50% discount for extra days
      final int numDiscountUnits = numTimeUnits - LONG_TERM_DISCOUNT_LIMIT;
      long discountPrice = numAreaUnits * numDiscountUnits * priceInCents / 2;
      addChargeBasisEntry(chargeBasisTagLongTerm, toChargeBasisUnit(pricePeriod), numDiscountUnits,
          numAreaUnits * priceInCents / 2,
          chargeBasisTextLongTerm, (int) discountPrice, explanationService.getExplanation(application));
      price += discountPrice;
    }
    setPriceInCents((int) price);
  }

  private void updateUrbanFarmingPrice() {
    // 2 EUR/sqm/term
    int numTerms = 1;
    if (application.getEndTime() != null && application.getStartTime() != null) {
      numTerms = application.getEndTime().getYear() - application.getStartTime().getYear() + 1;
    }

    final int urbanFarmingTermPrice = getPrice(PricingKey.URBAN_FARMING_TERM_PRICE);
    double billableArea = applicationArea == 0.0 ? 0.0 : Math.ceil(applicationArea);
    int netPrice = urbanFarmingTermPrice * (int) billableArea * numTerms;

    addChargeBasisEntry(ChargeBasisTag.ShortTermRentalUrbanFarming(), ChargeBasisUnit.SQUARE_METER, billableArea,
        urbanFarmingTermPrice * numTerms,
        InvoiceLines.URBAN_FARMING, netPrice, explanationService.getExplanation(application));
    setPriceInCents(netPrice);
  }

  private int getPrice(PricingKey key) {
    return pricingDao.findValue(ApplicationType.SHORT_TERM_RENTAL, key);
  }

  private String priceText(int priceInCents, String text) {
    final double euroPrice = priceInCents / 100.0;
    String priceText = decimalFormat.format(euroPrice);
    if (priceText.endsWith(",00")) {
      priceText = priceText.substring(0, priceText.length() - 3);
    }
    return String.format(text, priceText);
  }
}
