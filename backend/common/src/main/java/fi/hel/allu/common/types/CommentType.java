package fi.hel.allu.common.types;

/**
 * Valid comment types for application comments
 */
public enum CommentType {
  INVOICING,
  RETURN,
  REJECT,
  INTERNAL,
  PROPOSE_APPROVAL,
  PROPOSE_REJECT,
  EXTERNAL_SYSTEM
}
