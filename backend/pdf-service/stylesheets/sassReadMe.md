# Using SASS
Install Sass

```sh
gem install sass
```

Then do this

```sh
sass --watch style.sass:style.css
```

Then edit style.sass and be happy.
