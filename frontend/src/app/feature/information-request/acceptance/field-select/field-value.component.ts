import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {typeOfValue, ValueType} from '@util/object.util';
import {TimeUtil} from '@util/time.util';

@Component({
  selector: 'field-value',
  templateUrl: './field-value.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FieldValueComponent implements OnInit {
  valueType: ValueType;
  displayValue: any;

  ngOnInit(): void {
    this.valueType = typeOfValue(this.value);
  }

  @Input() set value(value: any) {
    if (value instanceof Date) {
      this.displayValue = TimeUtil.getUiDateString(value);
    } else {
      this.displayValue = value;
    }
  }
}
