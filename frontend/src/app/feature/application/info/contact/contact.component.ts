import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Contact} from '@model/customer/contact';
import {Some} from '@util/option';
import {NumberUtil} from '@util/number.util';
import {Observable} from 'rxjs';
import {CustomerWithContactsForm} from '@feature/customerregistry/customer/customer-with-contacts.form';
import {CustomerRoleType} from '@model/customer/customer-role-type';
import {ApplicationStore} from '@service/application/application-store';
import {ApplicationType} from '@model/application/type/application-type';
import {createDefaultOrdererId, fromOrdererId, toOrdererId} from '../cable-report/cable-report.form';
import {FormUtil} from '@util/form.util';
import {CustomerService} from '@service/customer/customer.service';
import {debounceTime, filter, map, switchMap, take, tap} from 'rxjs/internal/operators';
import {DistributionListEvents} from '@feature/application/distribution/distribution-list/distribution-list-events';
import {DistributionEntry} from '@model/common/distribution-entry';
import {DistributionType} from '@model/common/distribution-type';
import {OrdererId} from '@model/application/cable-report/orderer-id';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {findTranslation} from '@util/translations';
import {NotificationService} from '@feature/notification/notification.service';

const ALWAYS_ENABLED_FIELDS = ['id', 'name', 'customerId', 'orderer'];

@Component({
  selector: 'contact',
  viewProviders: [],
  templateUrl: './contact.component.html',
  styleUrls: [
    './contact.component.scss'
  ]
})
export class ContactComponent implements OnInit {
  @Input() parentForm: FormGroup;
  @Input() customerRoleType: string;
  @Input() readonly: boolean;
  @Input() contactRequired = false;

  form: FormGroup;
  contacts: FormArray;
  availableContacts: Observable<Array<Contact>>;
  matchingContacts: Observable<Array<Contact>>;
  showOrderer = false;

  private customerIdChanges = new BehaviorSubject<number>(undefined);

  constructor(private fb: FormBuilder,
              private customerService: CustomerService,
              private applicationStore: ApplicationStore,
              private distributionListEvents: DistributionListEvents,
              private notification: NotificationService) {}

  ngOnInit(): void {
    this.availableContacts = this.customerIdChanges.pipe(
      filter(id => NumberUtil.isDefined(id)),
      switchMap(id => this.customerService.findCustomerActiveContacts(id))
    );

    this.initContacts();
    this.showOrderer = ApplicationType.CABLE_REPORT === this.applicationStore.snapshot.application.type;

    if (this.readonly) {
      this.contacts.disable();
    }
  }

  contactSelected(contact: Contact, index: number): void {
    this.contacts.at(index).patchValue(contact);
    this.disableContactEdit(index);
  }

  canBeRemoved(): boolean {
    const contactCanBeRemoved = !this.contactRequired || (this.contacts.length > 1);
    const canBeEdited = !this.readonly;
    return contactCanBeRemoved && canBeEdited;
  }

  selectOrderer(index: number): void {
    const contact = this.contacts.at(index).value;
    const ordererId = OrdererId.of(contact.id, this.customerRoleType, index);
    this.parentForm.patchValue({ordererId: fromOrdererId(ordererId)});
  }

  isOrderer(index: number): boolean {
    const contact = this.contacts.at(index).value;
    return Some(this.parentForm.getRawValue().ordererId)
      .map(form => toOrdererId(form))
      .map(currentOrderer => currentOrderer.matches(contact.id, this.customerRoleType, index))
      .orElse(false);
  }

  /**
   * Resets form values if form contained existing contact
   */
  resetContactIfExisting(index: number): void {
    const contactCtrl = this.contacts.at(index);
    if (NumberUtil.isDefined(contactCtrl.value.id)) {
      contactCtrl.reset({
        name: contactCtrl.value.name,
        active: true
      });
    }
    contactCtrl.enable();
  }

  onKeyup(event: KeyboardEvent, index: number): void {
    if (event.code !== 'Enter') {
      this.resetContactIfExisting(index);
    }
  }

  onCustomerChange(customerId: number) {
    this.resetContacts();
    if (NumberUtil.isDefined(customerId)) {
      this.availableContacts = this.customerService.findCustomerActiveContacts(customerId);
    }
    this.customerIdChanges.next(customerId);
  }

  addContact(contact: Contact = new Contact()): void {
    const fg = Contact.formGroup(this.fb, contact);
    const nameControl = fg.get('name');
    this.matchingContacts = nameControl.valueChanges.pipe(
      debounceTime(300),
      switchMap(name => this.onNameSearchChange(name))
    );

    this.contacts.push(fg);

    if (NumberUtil.isDefined(contact.id)) {
      this.disableContactEdit(this.contacts.length - 1);
    }
  }

  showSaveContact$(contact: Contact): Observable<boolean> {
    const newContact = !NumberUtil.isExisting(contact);
    return this.customerIdChanges.pipe(
      take(1),
      map(id => NumberUtil.isDefined(id) && newContact)
    );
  }

  save(contact: Contact, index: number): void {
    this.customerIdChanges.pipe(
      filter(id => NumberUtil.isDefined(id)),
      take(1),
      switchMap(customerId => this.customerService.saveContact(customerId, contact))
    ).subscribe(saved => {
      this.notification.success(findTranslation('contact.action.save'));
      this.contactSelected(saved, index);
    });
  }

  /**
   * If customer is removed for some reason from form
   * and current customer contained contact which was orderer
   * then reset orderer
   */
  onCustomerRemove() {
    Some(this.parentForm.value.ordererId)
      .map(form => toOrdererId(form))
      .filter(ordererId => this.contacts.value.some(contact => ordererId.idOrRoleTypeMatches(contact.id, this.customerRoleType)))
      .do(ordererId => this.parentForm.patchValue({ordererId: createDefaultOrdererId()}));
  }

  remove(index: number): void {
    if (this.isOrderer(index)) {
      this.parentForm.patchValue({ordererId: createDefaultOrdererId()});
    }

    this.contacts.removeAt(index);
  }

  canBeAddedToDistribution(index: number): boolean {
    const contact = <FormGroup>this.contacts.at(index);
    const email = contact.getRawValue().email;
    return !this.readonly && !!email && email.length > 2;
  }

  addToDistribution(index: number): void {
    const contactFg = <FormGroup>this.contacts.at(index);
    const contact = contactFg.getRawValue();
    this.distributionListEvents.add(new DistributionEntry(null, contact.name, DistributionType.EMAIL, contact.email));
  }

  private onNameSearchChange(term: string): Observable<Array<Contact>> {
    if (!!term) {
      return this.availableContacts.pipe(
        map(contacts => contacts.filter(c => c.name.toLowerCase().indexOf(term.toLowerCase()) >= 0))
      );
    } else {
      return this.availableContacts;
    }
  }

  private initContacts(): void {
    this.form = <FormGroup>this.parentForm.get(CustomerWithContactsForm.formName(CustomerRoleType[this.customerRoleType]));
    this.contacts = <FormArray>this.form.get('contacts');
    const defaultContactList = this.contactRequired ? [new Contact()] : [];
    const roleType = CustomerRoleType[this.customerRoleType];

    this.applicationStore.application.pipe(
      map(app => app.customerWithContactsByRole(roleType)),
      tap(cwc => this.customerIdChanges.next(cwc.customerId)),
      map(cwc => cwc.contacts.length > 0 ? cwc.contacts : defaultContactList)
    ).subscribe(contacts => {
      FormUtil.clearArray(this.contacts);
      contacts.forEach(contact => this.addContact(contact));
    });
  }

  private resetContacts(): void {
    this.contacts.reset();
    while (this.contacts.length > 1) {
      this.remove(1);
    }
    this.contacts.enable();
  }

  private disableContactEdit(index: number): void {
    const contactCtrl = <FormGroup>this.contacts.at(index);
    Object.keys(contactCtrl.controls)
      .filter(key => ALWAYS_ENABLED_FIELDS.indexOf(key) < 0)
      .forEach(key => contactCtrl.get(key).disable());
  }
}
