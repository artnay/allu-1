/**
 * Class to wrap errors
 */
export class ErrorInfo {
  constructor(public title: string, public message?: string) {}
}
