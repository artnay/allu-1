export interface MapFeatureInfo {
  id: number;
  applicationId?: string;
  type?: string;
  startTime: string;
  endTime: string;
}
