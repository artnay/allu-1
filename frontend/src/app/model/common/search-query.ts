import {Sort} from './sort';

export interface SearchQuery {
  sort?: Sort;
}
