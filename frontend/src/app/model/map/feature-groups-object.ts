import {FeatureGroup} from 'leaflet';

export interface FeatureGroupsObject {
  [key: string]: FeatureGroup;
}
