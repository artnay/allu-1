export enum CustomerType {
  COMPANY,
  ASSOCIATION,
  PERSON,
  PROPERTY,
  OTHER
}
