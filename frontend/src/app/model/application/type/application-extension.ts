export abstract class ApplicationExtension {
  constructor(public applicationType?: string,
              public terms?: string) {
  }
}
